# FooBarQix

Kata for WeSquad hiring process

# Input

The input is a String. It can contains or not a positive or negative number.

# Rules

First Step : 

If the number is divisible by 3, write “Foo” instead of the number  
If the number is divisible by 5, add “Bar”  
If the number is divisible by 7, add “Qix”  
For each digit 3, 5, 7, add “Foo”, “Bar”, “Qix” in the digits order.  

As a personnal initiative, the rules also applies to negative numbers.

Second Step :

we must keep a trace of 0 in numbers, each 0 must be replaced by char “*“.

If the String does not contain an integer, an error is thrown.

# Installs

Install node on your machine  
run "npm install" to install the dependencies located in package.json

# Use

At the root of the project, run "npm start STRING MODE" replacing :  
- STRING by the string you want to test.  
- MODE by "single" if you want to test a single value or by "loop" if you want to loop over

To launch the unit tests (jest) located in main.test.js, run :

npm test

# Output

The output is displayed in the console.

# Example :

npm start 12 single

output :  
0 => FooBarQix*  
1 => 1  
2 => 2  
3 => FooFoo  
4 => 4  
5 => BarBar  
6 => Foo  
7 => QixQix  
8 => 8  
9 => Foo  
10 => Bar*  
11 => 11  
12 => Foo  

npm start 12 single

output:  
12 => Foo

npm start "12" single

output:  
12 => Foo

npm start "-1.2" single

output:  
Erreur : le paramètre doit être un entier.
