const displayFBQString = require("./src/displayFBQString");

const displayResultByMode = (str, mode) => {
  if (mode == "single") console.log(displayFBQString(str));
  else if (mode == "loop") {
    if (displayFBQString(str) == "Erreur : le paramètre doit être un entier")
      console.log(displayFBQString(str));
    else {
      let number = parseInt(str);
      console.log(number);
      if (number < 0) {
        for (var i = 0; i > number - 1; i--) {
          console.log(displayFBQString(String(i)));
        }
      } else {
        for (var j = 0; j < number + 1; j++) {
          console.log(displayFBQString(String(j)));
        }
      }
    }
  }
};

var num = process.argv[2];
var mode = process.argv[3];

displayResultByMode(num, mode);
