const displayFBQString = require("./src/displayFBQString");

describe("tests 0", () => {
  test("tests 0", () => {
    const zero = "0";
    expect(displayFBQString(zero)).toBe("0 => FooBarQix*");
  });
});

describe("tests Foo's", () => {
  test("correctly tests numbers divisible by 3 only and not containing any 3", () => {
    const numbers = ["9", "12", "18"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("9 => Foo");
    expect(printed[1]).toBe("12 => Foo");
    expect(printed[2]).toBe("18 => Foo");
  });
  test("correctly tests numbers divisible by 3 only and containing one 3", () => {
    const numbers = ["3", "93"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("3 => FooFoo");
    expect(printed[1]).toBe("93 => FooFoo");
  });
  test("correctly tests numbers divisible by 3 only and containing more than one 3", () => {
    const numbers = ["33", "363", "393"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("33 => FooFooFoo");
    expect(printed[1]).toBe("363 => FooFooFoo");
    expect(printed[2]).toBe("393 => FooFooFoo");
  });
});

describe("tests Bar's", () => {
  test("correctly tests numbers divisible by 5 only and containing one 5", () => {
    const numbers = ["25", "95"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("25 => BarBar");
    expect(printed[1]).toBe("95 => BarBar");
  });
  test("correctly tests numbers divisible by 5 only and containing more than one 5", () => {
    const numbers = ["55", "505"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("55 => BarBarBar");
    expect(printed[1]).toBe("505 => BarBar*Bar");
  });
});

describe("tests Qix's", () => {
  test("correctly tests numbers divisible by 7 only and not containing any 7", () => {
    const numbers = ["49", "91"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("49 => Qix");
    expect(printed[1]).toBe("91 => Qix");
  });
  test("correctly tests numbers divisible by 7 only and containing one 7", () => {
    const numbers = ["7", "217", "287"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("7 => QixQix");
    expect(printed[1]).toBe("217 => QixQix");
    expect(printed[2]).toBe("287 => QixQix");
  });
  test("correctly tests numbers divisible by 7 only and containing more than one 7", () => {
    const numbers = ["77", "7777"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("77 => QixQixQix");
    expect(printed[1]).toBe("7777 => QixQixQixQixQix");
  });
});

describe("tests divisibles by 3 and 5", () => {
  test("correctly tests numbers divisible by 3 and 5 only and containing no 3 and no 5", () => {
    const numbers = ["60", "120"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("60 => FooBar*");
    expect(printed[1]).toBe("120 => FooBar*");
  });
  test("correctly tests numbers divisible by 3 and 5 only and containing 3's and 5's", () => {
    const numbers = ["3285", "435"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("3285 => FooBarFooBar");
    expect(printed[1]).toBe("435 => FooBarFooBar");
  });
  test("correctly tests numbers divisible by 3 and 5 only and containing 3's and 7's", () => {
    const numbers = ["7320", "3270"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("7320 => FooBarQixFoo*");
    expect(printed[1]).toBe("3270 => FooBarFooQix*");
  });
  test("correctly tests numbers divisible by 3 and 5 only and containing 5's and 7's", () => {
    const numbers = ["75", "5175"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("75 => FooBarQixBar");
    expect(printed[1]).toBe("5175 => FooBarBarQixBar");
  });
});

describe("tests divisibles by 3 and 7", () => {
  test("correctly tests numbers divisible by 3 and 7 only and containing no 3 and no 7", () => {
    const number = "189";
    const printed = displayFBQString(number);
    expect(printed).toBe("189 => FooQix");
  });
  test("correctly tests numbers divisible by 3 and 7 only and containing 3's and 7's", () => {
    const number = "27433728";
    const printed = displayFBQString(number);
    expect(printed).toBe("27433728 => FooQixQixFooFooQix");
  });
  test("correctly tests numbers divisible by 3 and 7 only and containing 3's and 5's", () => {
    const number = "5103";
    const printed = displayFBQString(number);
    expect(printed).toBe("5103 => FooQixBar*Foo");
  });
  test("correctly tests numbers divisible by 3 and 7 only and containing 5's and 7's", () => {
    const number = "567";
    const printed = displayFBQString(number);
    expect(printed).toBe("567 => FooQixBarQix");
  });
});

describe("tests divisibles by 5 and 7", () => {
  test("correctly tests numbers divisible by 5 and 7 only and containing no 5 and no 7", () => {
    const numbers = ["140", "280"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("140 => BarQix*");
    expect(printed[1]).toBe("280 => BarQix*");
  });
  test("correctly tests numbers divisible by 5 and 7 only and containing 5's and 7's", () => {
    const number = "76475";
    const printed = displayFBQString(number);
    expect(printed).toBe("76475 => BarQixQixQixBar");
  });
  test("correctly tests numbers divisible by 5 and 7 only and containing 3's and 7's", () => {
    const number = "1934870";
    const printed = displayFBQString(number);
    expect(printed).toBe("1934870 => BarQixFooQix*");
  });
  test("correctly tests numbers divisible by 5 and 7 only and containing 3's and 5's", () => {
    const number = "35";
    const printed = displayFBQString(number);
    expect(printed).toBe("35 => BarQixFooBar");
  });
});

describe("tests divisibles by 3, 5 and 7", () => {
  test("correctly tests numbers divisible by 3, 5 and 7 ", () => {
    const numbers = ["105", "315", "7035"];
    const printed = [];
    for (number of numbers) printed.push(displayFBQString(number));
    expect(printed[0]).toBe("105 => FooBarQix*Bar");
    expect(printed[1]).toBe("315 => FooBarQixFooBar");
    expect(printed[2]).toBe("7035 => FooBarQixQix*FooBar");
  });
});

describe("tests negatives", () => {
  test("tests negatives", () => {
    const negativeInt = "-3";
    expect(displayFBQString(negativeInt)).toBe("-3 => FooFoo");
  });
});

describe("tests non integers", () => {
  test("tests floats", () => {
    const float = "3.4";
    expect(displayFBQString(float)).toBe("Erreur : le paramètre doit être un entier");
  });
  test("tests strings", () => {
    const string = "HEY";
    expect(displayFBQString(string)).toBe("Erreur : le paramètre doit être un entier");
  });
});

describe("tests presence of 0's", () => {
  test("tests presence of 0's when no divisibility and no 3, 5 or 7", () => {
    const number = "101";
    expect(displayFBQString(number)).toBe("101 => 1*1");
  });
  test("tests presence of 0's when there is divisibility and no 3, 5 or 7", () => {
    const number = "90";
    expect(displayFBQString(number)).toBe("90 => FooBar*");
  });
  test("tests presence of 0's when no divisibility and at least a 3, 5 or 7", () => {
    const number = "70";
    expect(displayFBQString(number)).toBe("70 => BarQixQix*");
  });
});
