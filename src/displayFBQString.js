const isDivisibleBy3 = require("./isDivisibleBy3");
const isDivisibleBy5 = require("./isDivisibleBy5");
const isDivisibleBy7 = require("./isDivisibleBy7");

//Displays Foo, Bar and Qix for the number given as parameter. See the README for more details.
const displayFBQString = (numberAsString) => {
  let result = ["", ""];
  let printed = "";
  let absoluteNumberAsString = "";
  let contains357 = false;

  //Keep the absolute value of number for Foo Bar Qix testing
  numberAsString[0] == "-"
    ? (absoluteNumberAsString = numberAsString.substring(1))
    : (absoluteNumberAsString = numberAsString);

  for (let char of absoluteNumberAsString) {
    //Check if each character of the parameter string is a digit
    if (char >= "0" && char <= "9") {
      //Convert char into digit
      let digit = parseInt(char);

      //Do the replacements
      switch (digit) {
        case 0:
          result[1] += "*";
          break;
        case 1:
          result[1] += "1";
          break;
        case 2:
          result[1] += "2";
          break;
        case 3:
          result[1] += "Foo";
          contains357 = true;
          break;
        case 4:
          result[1] += "4";
          break;
        case 5:
          result[1] += "Bar";
          contains357 = true;
          break;
        case 6:
          result[1] += "6";
          break;
        case 7:
          result[1] += "Qix";
          contains357 = true;
          break;
        case 8:
          result[1] += "8";
          break;
        case 9:
          result[1] += "9";
          break;
        default:
          result[1] += "";
      }
    } else return "Erreur : le paramètre doit être un entier";
  }

  //Parse the initial string to test divisiblity
  const parsedString = parseInt(numberAsString);

  if (isDivisibleBy3(parsedString)) {
    result[0] += "Foo";
  }
  if (isDivisibleBy5(parsedString)) {
    result[0] += "Bar";
  }
  if (isDivisibleBy7(parsedString)) {
    result[0] += "Qix";
  }

  let filteredDigits = [];

  //Keeps digits depending on divisibility by 3 5 or 7 and presence of 3 5 and 7
  if (result[0] == "") {
    if (!contains357) {
      filteredDigits = result[1];
    } else {
      filteredDigits = result[1].replace(/1|2|4|5|6|8|9/gi, "");
    }
  } else {
    filteredDigits = result[1].replace(/1|2|4|5|6|8|9/gi, "");
  }

  //Concats the string of divisibility FBQ replacements and the string of digit FBQ replacement
  printed = parsedString + " => " + result[0] + filteredDigits;
  return printed;
};

module.exports = displayFBQString;
