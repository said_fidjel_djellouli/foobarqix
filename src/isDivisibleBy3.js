const isDivisibleBy3 = (number) => {
    return number % 3 === 0;
  };
  
  module.exports = isDivisibleBy3;
  