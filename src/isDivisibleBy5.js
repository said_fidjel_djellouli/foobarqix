const isDivisibleBy5 = (number) => {
    return number % 5 === 0;
  };
  
  module.exports = isDivisibleBy5;
  