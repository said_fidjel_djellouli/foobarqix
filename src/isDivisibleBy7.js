const isDivisibleBy7 = (number) => {
    return number % 7 === 0;
  };
  
  module.exports = isDivisibleBy7;
  